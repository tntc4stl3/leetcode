# Only three cases will be true.
# 1. All upper
# 2. All lower
# 3. Fist char upper, the rest lower.

class Solution(object):
    def detectCapitalUse(self, word):
        """
        :type word: str
        :rtype: bool
        """
        return word.isupper() or word.islower() or (word[0].isupper() and word[1:].islower())