import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_detect_capital_use(self):
        self.assertEqual(self.solution.detectCapitalUse('USA'), True)
        self.assertEqual(self.solution.detectCapitalUse('hello'), True)
        self.assertEqual(self.solution.detectCapitalUse('Hello'), True)
        self.assertEqual(self.solution.detectCapitalUse('U'), True)
        self.assertEqual(self.solution.detectCapitalUse('HellO'), False)


if __name__ == '__main__':
    unittest.main()
