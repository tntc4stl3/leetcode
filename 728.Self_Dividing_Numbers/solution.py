# A self-dividing number is a number that is divisible by every digit it contains.
#
# For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.
#
# Also, a self-dividing number is not allowed to contain the digit zero.
#
# Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.
#
# Example 1:
# Input:
# left = 1, right = 22
# Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
# Note:
#
# The boundaries of each input argument are 1 <= left <= right <= 10000.


class Solution:
    @staticmethod
    def get_all_numbers(num):
        nums = []
        while num != 0:
            nums.append(num % 10)
            num /= 10
        return nums

    def is_self_divide_number(self, num):
        nums = self.get_all_numbers(num)
        return 0 not in nums and all(map(lambda x: num % x == 0, nums))

    def selfDividingNumbers(self, left, right):
        """
        :type left: int
        :type right: int
        :rtype: List[int]
        """
        return filter(self.is_self_divide_number, range(left, right+1))


if __name__ == '__main__':
    solution = Solution()
    assert solution.selfDividingNumbers(1, 22) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]