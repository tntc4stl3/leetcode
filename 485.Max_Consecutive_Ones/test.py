import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_find_max_consecutive_ones(self):
        self.assertEqual(
            self.solution.findMaxConsecutiveOnes([1, 1, 0, 1, 1, 1]),
            3)
        self.assertEqual(
            self.solution.findMaxConsecutiveOnes([1]),
            1)
        self.assertEqual(
            self.solution.findMaxConsecutiveOnes([0]),
            0)


if __name__ == '__main__':
    unittest.main()
