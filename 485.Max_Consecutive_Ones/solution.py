class Solution(object):
    def findMaxConsecutiveOnes(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_counts = counts = 0
        for num in nums:
            if num == 1:
                counts += 1
                if counts > max_counts:
                    max_counts = counts
            else:
                counts = 0
        return max_counts
