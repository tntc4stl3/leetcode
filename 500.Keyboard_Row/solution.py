import re


class Solution(object):
    def findWords(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        ret = []
        for word in words:
            if re.search('^([qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*)$', word, re.I):
                ret.append(word)
        return ret
