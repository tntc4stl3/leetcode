import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_find_words(self):
        self.assertEqual(
            self.solution.findWords(["Hello","Alaska","Dad","Peace"]),
            ["Alaska", "Dad"]
        )


if __name__ == '__main__':
    unittest.main()
