"""
Given a positive integer, return its corresponding column title as appear in an Excel sheet.

For example:

    1 -> A
    2 -> B
    3 -> C
    ...
    26 -> Z
    27 -> AA
    28 -> AB
"""


import string
class Solution(object):
    def convertToTitle(self, n):
        """
        :type n: int
        :rtype: str
        """
        title = []
        while n != 0:
            num = n % 26
            title.append(string.ascii_uppercase[num-1])
            n /= 26
            if num == 0:
                n -= 1
        return ''.join(title[::-1])