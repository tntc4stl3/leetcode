class Solution {
    public int numJewelsInStones(String J, String S) {
        int[] aCharFlag = new int[256];
        int count = 0;
        for(int i = 0; i < J.length(); i++) {
            char c = J.charAt(i);
            aCharFlag[Integer.valueOf(c)] = 1;
        }
        for(int i = 0; i < S.length(); i++) {
            char c = S.charAt(i);
            if (aCharFlag[Integer.valueOf(c)] == 1) {
                count++;
            }
        }
        return count;
    }
}
