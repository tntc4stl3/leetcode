import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_island_perimeter(self):
        self.assertEqual(
            self.solution.islandPerimeter([[0,1,0,0],[1,1,1,0],[0,1,0,0],[1,1,0,0]]),
            16)


if __name__ == '__main__':
    unittest.main()
