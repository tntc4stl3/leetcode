class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        count = 0
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if grid[i][j] == 1:
                    surround = (i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)
                    for x, y in surround:
                        if x < 0 or x == len(grid) or y < 0 or y == len(grid[i]) or grid[x][y] == 0:
                            count += 1
        return count
