#!/usr/bin/env python
# encoding: utf-8

class Solution(object):
    def findTheDifference(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: str
        """
        s_dict = {}
        for c in s:
            s_dict[c] = s_dict.get(c, 0) + 1

        for c in t:
            if c not in s_dict.keys():
                return c
            else:
                s_dict[c] -= 1
                if s_dict[c] < 0:
                    return c


if __name__ == '__main__':
    case = Solution()
    print case.findTheDifference('abcd', 'abcde')
