import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_solution(self):
        self.assertEqual(self.solution.findComplement(5), 2)
        self.assertEqual(self.solution.findComplement(1), 0)


if __name__ == '__main__':
    unittest.main()
