#!/usr/bin/env python
# encoding: utf-8

"""
You are playing the following Nim Game with your friend: There is a heap of stones on the table, each time one of you take turns to remove 1 to 3 stones. The one who removes the last stone will be the winner. You will take the first turn to remove the stones.

Both of you are very clever and have optimal strategies for the game. Write a function to determine whether you can win the game given the number of stones in the heap.

For example, if there are 4 stones in the heap, then you will never win the game: no matter 1, 2, or 3 stones you remove, the last stone will always be removed by your friend.
"""


"""
根据分析，如果堆里有4个石头，谁先手谁输，所以策略就是要保证最后一轮的时候堆里
只有4个石头并且是对方先手，由于每次都是我方先手，最简单的办法就是，在拿完之后
让剩下的石头为4的倍数，这样假设对方拿x个石头，我方就拿4-x个石头，就能保证胜利
"""

class Solution(object):
    def canWinNim(self, n):
        """
        :type n: int
        :rtype: bool
        """
        return n % 4 != 0

