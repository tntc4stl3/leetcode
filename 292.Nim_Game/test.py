import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_nim_number(self):
        self.assertEqual(self.solution.canWinNim(4), False)
        self.assertEqual(self.solution.canWinNim(1), True)
        self.assertEqual(self.solution.canWinNim(7), True)


if __name__ == '__main__':
    unittest.main()
