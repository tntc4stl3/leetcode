"""
Given an array of integers, every element appears twice except for one. Find that single one.

Note:
Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
"""

class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        count = {}
        for num in nums:
            count.setdefault(num, 0)
            count[num] += 1

        for key, value in count.iteritems():
            if value == 1:
                return key

    def xor_solution(self, nums):
        """
        Best solution on leetcode
        Use XOR because A ^ A = 0
        """
        result = 0
        for i in nums:
            result ^= i
        return result
