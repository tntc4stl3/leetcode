"""
Given an array of size n, find the majority element. The majority element is the element that appears more than n / 2 times.

You may assume that the array is non-empty and the majority element always exist in the array.
"""


class Solution(object):
    def majorityElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        element_count = {}
        major_count = len(nums) / 2
        for num in nums:
            element_count[num] = element_count.setdefault(num, 0) + 1
            if element_count[num] > major_count:
                return num