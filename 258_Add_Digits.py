"""
Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.

For example:

Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. Since 2 has only one digit, return it.
"""


class Solution(object):
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        sum = 0
        while num != 0:
            sum += num % 10
            num /= 10
        if sum < 10:
            return sum
        else:
            return self.addDigits(sum)

    def o1_solution(self, num):
        """
        First you should understand:

        10^k % 9 = 1
        a*10^k % 9 = a % 9
        Then let's use an example to help explain.

        Say a number x = 23456

        x = 2* 10000 + 3 * 1000 + 4 * 100 + 5 * 10 + 6

        2 * 10000 % 9 = 2 % 9

        3 * 1000 % 9 = 3 % 9

        4 * 100 % 9 = 4 % 9

        5 * 10 % 9 = 5 % 9

        Then x % 9 = ( 2+ 3 + 4 + 5 + 6) % 9, note that x = 2* 10000 + 3 * 1000 + 4 * 100 + 5 * 10 + 6

        So we have 23456 % 9 = (2 + 3 + 4 + 5 + 6) % 9
        """
        sum = num % 9
        if sum == 0 and num > 0:
            return 9
        else:
            return sum
