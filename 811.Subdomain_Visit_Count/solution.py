from collections import defaultdict

class Solution(object):
    def subdomainVisits(self, cpdomains):
        """
        :type cpdomains: List[str]
        :rtype: List[str]
        """
        counts = defaultdict(int)
        for item in cpdomains:
            count, domain = item.split(' ')
            subdomains = domain.split('.')
            for i in range(len(subdomains)):
                counts['.'.join(subdomains[i:])] += int(count)

        return ["%s %s" % (v, k) for (k, v) in counts.items()]