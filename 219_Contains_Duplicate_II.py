"""
Given an array of integers and an integer k, find out whether there are two distinct indices i and j in the array such that nums[i] = nums[j] and the difference between i and j is at most k.
"""


class Solution(object):
    def containsNearbyDuplicate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """
        """
        0 ~ k
        1 ~ k + 1
        len(nums) - k -1  ~ len(nums) - 1
        """
        num_count = {}
        for num in nums[0:k+1]:
            num_count[num] = num_count.setdefault(num, 0) + 1
            if num_count[num] > 1:
                return True
        for i in range(1, len(nums) - k):
            num_count[nums[i-1]] -= 1
            num_count[nums[i+k]] = num_count.setdefault(nums[i+k], 0) + 1
            if num_count[nums[i+k]] > 1:
                return True
        return False