import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_dict_solution(self):
        self.assertEqual(self.solution.findTheDifference('abcd', 'abcde'), 'e')
        self.assertEqual(self.solution.findTheDifference('abcd', 'cbead'), 'e')

    def test_difference_solution(self):
        self.assertEqual(self.solution.difference_way('abcd', 'abcde'), 'e')

    def test_xor_solution(self):
        self.assertEqual(self.solution.xor_way('abcd', 'abcde'), 'e')


if __name__ == '__main__':
    unittest.main()