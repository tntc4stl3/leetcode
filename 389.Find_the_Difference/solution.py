#!/usr/bin/env python
# encoding: utf-8

class Solution(object):
    def findTheDifference(self, s, t):
        """
        Dictionary way.
        :type s: str
        :type t: str
        :rtype: str
        """
        s_dict = {}
        for c in s:
            s_dict[c] = s_dict.get(c, 0) + 1

        for c in t:
            if c not in s_dict.keys():
                return c
            else:
                s_dict[c] -= 1
                if s_dict[c] < 0:
                    return c

    def difference_way(self, s, t):
        """
        Use ord to convert character to integer.
        :param s:
        :param t:
        :return:
        """
        diff = 0
        for i in range(len(s)):
            diff -= ord(s[i])
            diff += ord(s[i])
        diff += ord(t[-1])
        return chr(diff)


    def xor_way(self, s, t):
        """XOR way"""
        code = 0
        for c in s + t:
            code ^= ord(c)
        return chr(code)
