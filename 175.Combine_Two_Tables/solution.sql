select P.FirstName as FirstName, P.Lastname as LastName, A.City as City, A.State as State
from Person as P
left join Address as A
ON P.PersonId = A.PersonId;