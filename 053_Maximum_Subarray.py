"""
Find the contiguous subarray within an array (containing at least one number) which has the largest sum.

For example, given the array [-2,1,-3,4,-1,2,1,-5,4],
the contiguous subarray [4,-1,2,1] has the largest sum = 6.
"""


class Solution(object):
    def maxSubArray(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        max_sum = max_now = nums[0]
        for num in nums[1:]:
            if max_now <= 0:
                max_now = num
            else:
                max_now += num

            if max_now > max_sum:
                max_sum = max_now

        return max_sum