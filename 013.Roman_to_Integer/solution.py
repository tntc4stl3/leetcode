"""
Given a roman numeral, convert it to an integer.

Input is guaranteed to be within the range from 1 to 3999.
"""


class Solution(object):
    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        # for example:
        # CM is 900
        # DCCC is 800
        # CD is 400
        result = 0
        for c in s[::-1]:
            if c == 'I':
                result = result - 1 if result >= 5 else result + 1
            elif c == 'V':
                result += 5
            elif c == 'X':
                result = result - 10 if result >= 50 else result + 10
            elif c == 'L':
                result += 50
            elif c == 'C':
                result = result - 100 if result >= 500 else result + 100
            elif c == 'D':
                result += 500
            elif c == 'M':
                result += 1000

        return result