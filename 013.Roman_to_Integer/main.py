import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_solution(self):
        self.assertEqual(self.solution.romanToInt("CM"), 900)
        self.assertEqual(self.solution.romanToInt("DCCC"), 800)
        self.assertEqual(self.solution.romanToInt("CD"), 400)


if __name__ == '__main__':
    unittest.main()
