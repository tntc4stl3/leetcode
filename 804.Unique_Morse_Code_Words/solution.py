class Solution(object):

    def __init__(self):
        lowercase = 'abcdefghijklmnopqrstuvwxyz'
        morse_code = [".-","-...","-.-.","-..",".","..-.","--.","....","..",
                     ".---","-.-",".-..","--","-.","---",".--.","--.-",".-.",
                     "...","-","..-","...-",".--","-..-","-.--","--.."]
        self.mapping = {k: v for k, v in zip(lowercase, morse_code)}

    def uniqueMorseRepresentations(self, words):
        """
        :type words: List[str]
        :rtype: int
        """
        morse_set = set()
        for word in words:
            morse_set.add(''.join([self.mapping[c] for c in word]))

        return len(morse_set)


if __name__ == '__main__':
    solution = Solution()
    assert solution.uniqueMorseRepresentations(["gin", "zen", "gig", "msg"]) == 2