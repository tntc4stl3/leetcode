"""
Write a function that takes a string as input and reverse only the vowels of a string.

Example 1:
Given s = "hello", return "holle".

Example 2:
Given s = "leetcode", return "leotcede".

Note:
The vowels does not include the letter "y".
"""

class Solution(object):
    def is_vowel(self, c):
        return c.lower() in 'aeiou'

    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        input = list(s)
        i = 0
        j = len(input) - 1
        while i < j:
            if self.is_vowel(input[i]) and self.is_vowel(input[j]):
                input[i], input[j] = input[j], input[i]
                i += 1
                j -= 1
            elif self.is_vowel(input[i]):
                j -= 1
            elif self.is_vowel(input[j]):
                i += 1
            else:
                i += 1
                j -= 1

        return ''.join(input)