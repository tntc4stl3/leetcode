#!/usr/bin/env python
# encoding: utf-8

class Solution(object):
    def fizzBuzz(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        result = []
        for i in range(1, n+1):
            if i % 3 == 0:
                temp = "Fizz"
                if i % 5 == 0:
                    temp += "Buzz"
            elif i % 5 == 0:
                temp = "Buzz"
            else:
                temp = str(i)
            result.append(temp)
        return result
