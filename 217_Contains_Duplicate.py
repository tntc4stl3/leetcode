"""
Given an array of integers, find if the array contains any duplicates. Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.
"""


class Solution(object):
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        check_dict = {}
        for num in nums:
            check_dict[num] = check_dict.setdefault(num, 0) + 1
            if check_dict[num] > 1:
                return True
        return False