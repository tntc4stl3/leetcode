class Solution(object):
    def findDisappearedNumbers(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        return sorted(list(set(range(1, len(nums) + 1)) - set(nums)))

    def o_n_solution(self, nums):
        ret = []

        # first loop, use number as index and make the number on that
        # index-1 as a negative one.
        for num in nums:
            nums[abs(num)-1] = -nums[abs(num)-1]

        # loop again, if the number is positive, the index+1 is what we want.
        for i, num in enumerate(nums):
            if num > 0:
                ret.append(i + 1)

        return ret
