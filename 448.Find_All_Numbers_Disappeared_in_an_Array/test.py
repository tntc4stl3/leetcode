import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_find_disappeared_numbers(self):
        self.assertEqual(
            self.solution.findDisappearedNumbers([4,3,2,7,8,2,3,1]),
            [5, 6])

    def test_o_n_solution(self):
        self.assertEqual(
            self.solution.findDisappearedNumbers([4,3,2,7,8,2,3,1]),
            [5, 6])


if __name__ == '__main__':
    unittest.main()
