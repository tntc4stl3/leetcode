"""
Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
"""

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):
    def sortedArrayToBST(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        def convert(start, end):
            if start > end:
                return None
            else:
                mid = (start + end) / 2
                root = TreeNode(nums[mid])
                root.left = convert(start, mid-1)
                root.right = convert(mid+1, end)

        root = convert(0, len(nums) - 1)
        return root