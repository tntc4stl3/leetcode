class Solution {
	public int countBits(int num) {
		int count = 0;
		while(num != 0) {
			num = num & (num - 1);
			count++;
		}
		return count;
	}
	
	public boolean isPrime(int num) {
        if (num == 1) {
            return false;
        }
		for(int i = 2; i <= Math.sqrt(num); i++) {
			if (num % i == 0) {
				return false;
			}
		}
		return true;
	}
	
    public int countPrimeSetBits(int L, int R) {
        int count = 0;
        for(int i = L; i <= R; i++) {
        	if (this.isPrime(this.countBits(i))) {
        		count++;
        	}
        }
        return count;
    }
}