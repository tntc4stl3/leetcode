# Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.
#
# The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are R (Right), L (Left), U (Up) and D (down). The output should be true or false representing whether the robot makes a circle.
#
# Example 1:
# Input: "UD"
# Output: true
# Example 2:
# Input: "LL"
# Output: false


class Solution:
    def __init__(self):
        self.directions = {
            'U': [0, 1],
            'D': [0, -1],
            'L': [-1, 0],
            'R': [1, 0]
        }

    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        origin = [0, 0]
        for ch in moves:
            direction = self.directions[ch]
            origin[0] += direction[0]
            origin[1] += direction[1]

        return origin[0] == 0 and origin[1] == 0


if __name__ == "__main__":
    solution = Solution()
    assert solution.judgeCircle('UD') is True
    assert solution.judgeCircle('LL') is False
