import unittest

from solution import Solution


class SolutionTestCase(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_solution(self):
        self.assertFalse(self.solution.twoSum([4], 8))
        self.assertFalse(self.solution.twoSum([3, 4, 5], 10))
        self.assertEqual(self.solution.twoSum([2, 7, 11, 15], 9), [0, 1])


if __name__ == '__main__':
    unittest.main()
