class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        if len(nums) <= 1:
            return False
        temp_dict = {}
        for i in range(len(nums)):
            if nums[i] in temp_dict:
                return [temp_dict[nums[i]], i]
            else:
                temp_dict[target - nums[i]] = i
