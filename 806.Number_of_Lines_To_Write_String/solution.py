class Solution(object):
    def numberOfLines(self, widths, S):
        """
        :type widths: List[int]
        :type S: str
        :rtype: List[int]
        """
        mapping = {k: v for k, v in zip('abcdefghijklmnopqrstuvwxyz', widths)}

        count = 0
        line = 1
        for c in S:
            if count + mapping[c] <= 100:
                count += mapping[c]
            else:
                line += 1
                count = mapping[c]

        return [line, count]