package main

import "fmt"

func hammingDistance(x int, y int) int {
	z := x ^ y
	count := 0
	for z > 0 {
		if z%2 == 1 {
			count++
		}
		z /= 2
	}
	return count
}

func main() {
	fmt.Println(hammingDistance(1, 4))
}
