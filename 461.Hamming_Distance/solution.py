class Solution(object):
    def hammingDistance(self, x, y):
        """
        :type x: int
        :type y: int
        :rtype: int
        """
        bin_x = bin(x)[2:]
        bin_y = bin(y)[2:]
        if len(bin_x) > len(bin_y):
            bin_y = bin_y.zfill(len(bin_x))
        else:
            bin_x = bin_x.zfill(len(bin_y))

        count = 0
        for a, b in zip(bin_x, bin_y):
            if a != b:
                count += 1
        return count

    def one_line_solution(self, x, y):
        return bin(x ^ y).count('1')
