import unittest

from solution import Solution


class TestSolution(unittest.TestCase):
    def setUp(self):
        self.solution = Solution()

    def test_solution(self):
        self.assertEqual(self.solution.hammingDistance(1, 4), 2)

    def test_one_line_solution(self):
        self.assertEqual(self.solution.one_line_solution(1, 4), 2)


if __name__ == '__main__':
    unittest.main()
