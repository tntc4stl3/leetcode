"""
Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.
"""

class Solution(object):
    def firstUniqChar(self, s):
        """
        :type s: str
        :rtype: int
        """
        record = {}

        for i, c in enumerate(s):
            if c in record.keys():
                record[c][0] += 1
            else:
                record[c] = [1, i]

        indexes = sorted([v[1] for k, v in record.iteritems() if v[0] == 1])
        return indexes[0] if indexes else -1